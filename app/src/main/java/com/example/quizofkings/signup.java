package com.example.quizofkings;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.IOException;

public class signup extends AppCompatActivity {
    static String username;
    static String password;
    boolean ok1 = false;
    boolean ok2 = false;
    boolean ok3 = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        final EditText username_edittext = findViewById(R.id.username_signUp);
        final EditText password_edittext = findViewById(R.id.password_signup);
        final EditText confirm_password = findViewById(R.id.confirm_signup);
        final TextView message_textview = findViewById(R.id.message);
        Button submit_button = findViewById(R.id.submit_button);



        username_edittext.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b){
                    Thread thread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                String input_username = username_edittext.getText().toString();
                                login_signup.dos.writeUTF(input_username);
                                String answer = login_signup.dis.readUTF();
                                if (!answer.equals("ok")){
                                    message_textview.setText("this username is already existed");
                                }else {
                                    username=input_username;
                                    ok1=true;
                                }

                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                        }
                    });



                    thread.start();
                }
            }
        });

        password_edittext.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b){
                    String input_password = password_edittext.getText().toString();
                    if (input_password.length() < 5){
                        message_textview.setText("password is short");
                    }else {
                        password = input_password;
                        ok2=true;
                    }


                }
            }
        });

        confirm_password.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b){
                    String password = password_edittext.getText().toString();
                    String confirm = confirm_password.getText().toString();
                    if (!password.equals(confirm)){
                        message_textview.setText("password and confirm are not equal");
                    }else {
                        ok3=true;
                    }
                }
            }
        });

        final Intent intent = new Intent(getApplicationContext() , MainActivity.class);
        submit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (ok1 && ok2 && ok3){
                                login_signup.dos.writeUTF(password);
                                startActivity(intent);
                                login_signup.user_username = username;
                                login_signup.user_password = password;
                            }

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
                thread.start();
            }
        });


    }
}