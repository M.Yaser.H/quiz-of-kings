package com.example.quizofkings;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.logging.SocketHandler;

public class login_signup  extends AppCompatActivity {
    public Socket socketfirst;
    public static SocketHandler socket;
    public static DataOutputStream dos ;
    public static DataInputStream dis;
    public static String user_username = "";
    public static String user_password = "";
    public static int guest_number = 1;
    public static boolean is_guest = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_signup);
        Button login_button =findViewById(R.id.login_button);
        Button signup_button =findViewById(R.id.signup_button);
        Button guest_button = findViewById(R.id.guest);

        final Intent login = new Intent(getApplicationContext(),Login.class);
        final Intent signup = new Intent(getApplicationContext(),signup.class);
        final Intent guest = new Intent(getApplicationContext(),MainActivity.class);


        Thread thread =new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    socketfirst = new Socket("10.0.2.2", 6500);
                    dis = new DataInputStream(socketfirst.getInputStream());
                    dos = new DataOutputStream(socketfirst.getOutputStream());


//                    socket.setSocket(socket);

                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        thread.start();



        login_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Thread thread2 = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            dos.writeUTF("login");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
                thread2.start();
                startActivity(login);


            }
        });

        final Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    dos.writeUTF("signup");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        signup_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                thread1.start();
                startActivity(signup);

            }
        });

        guest_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                user_username = "guest"+guest_number;
                guest_number+=1;
                is_guest=true;
                startActivity(guest);
            }
        });

    }
}