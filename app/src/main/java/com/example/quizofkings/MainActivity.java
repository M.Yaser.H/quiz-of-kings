package com.example.quizofkings;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.quizofkings.fragments.Home_fragment;
import com.example.quizofkings.fragments.Profile_fragment;
import com.example.quizofkings.fragments.Setting_fragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {
    private  BottomNavigationView bottomNavigation ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bottomNavigation = findViewById(R.id.buttom_navigationbar);
        Home_fragment home_fragment = new Home_fragment();
        open_fragment(home_fragment);

        bottomNavigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.home_nav:
                        Home_fragment home_fragment = new Home_fragment();
                        open_fragment(home_fragment);
//                        Toast.makeText(getApplicationContext(),"clicked on home",Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.setting_nav:
                        Setting_fragment setting_fragment = new Setting_fragment();
                        open_fragment(setting_fragment);
//                        Toast.makeText(getApplicationContext(),"clicked on setting",Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.profile_nav:
                        Profile_fragment profile_fragment = new Profile_fragment();
                        open_fragment(profile_fragment);
//                        Toast.makeText(getApplicationContext(),"clicked on profile",Toast.LENGTH_SHORT).show();
                        break;
                }
                return true;
            }
        });


    }
    public void open_fragment(Fragment fragment){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container,fragment);
//        transaction.addToBackStack(null);
        transaction.commit();
    }
}