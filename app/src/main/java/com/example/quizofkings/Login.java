package com.example.quizofkings;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.IOException;

public class Login extends AppCompatActivity {
    boolean ok = false;
    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        final EditText username = findViewById(R.id.username_login);
        final EditText password = findViewById(R.id.password_login);
        Button ok_button = findViewById(R.id.ok_button_login);
        final TextView message = findViewById(R.id.message_textView_login);
        final Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        ok_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(intent);
            }
        });


        ok_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Thread thread = new Thread(new Runnable() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void run() {

                        final String username1 = username.getText().toString();
                        final String password1 = password.getText().toString();

                        try {
                            if (!ok) {
                                login_signup.dos.writeUTF(username1);
                                login_signup.dos.writeUTF(password1);
                                String answer = login_signup.dis.readUTF();
                                if (answer.equals("ok")) {
                                    startActivity(intent);
                                    login_signup.user_username = username1;
                                    login_signup.user_password = password1;
                                } else {
                                    message.setText("Username or Password is incorrect");
                                }
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
                thread.start();
            }
        });
    }
}