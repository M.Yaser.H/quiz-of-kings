package com.example.quizofkings;

import java.net.Socket;

public class SocketHandler {
    private static Socket socket;

    public   Socket getSocket(){
        return socket;
    }

    public    void setSocket(Socket socket){
        SocketHandler.socket = socket;
    }
}
