package com.example.quizofkings;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.IOException;

public class Change_password extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        final EditText new_password_input = findViewById(R.id.change_password_editText);
        Button submit = findViewById(R.id.submit_button_change_password);
        final TextView error_textview = findViewById(R.id.error_textView_change_password);

        final Intent intent = new Intent(getApplicationContext(),MainActivity.class);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String password = new_password_input.getText().toString();
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (password.length() > 5){
                                login_signup.dos.writeUTF(login_signup.user_username);
                                login_signup.dos.writeUTF(password);
                                login_signup.user_password = password;
                                startActivity(intent);

                            }else {
                                error_textview.setText("the password is too short");
                            }

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
                thread.start();



            }
        });

    }
}