package com.example.quizofkings;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.IOException;

public class Change_username extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_username);
        final EditText new_username_input = findViewById(R.id.change_username_editText);
        Button submit = findViewById(R.id.submit_button_change_username);
        final TextView error_textview = findViewById(R.id.error_textView_change_username);

        final Intent intent = new Intent(getApplicationContext(),MainActivity.class);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String username = new_username_input.getText().toString();
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (username.length() > 1){
                                login_signup.dos.writeUTF(login_signup.user_username);
                                login_signup.dos.writeUTF(username);
                                String answer = login_signup.dis.readUTF();
                                if (answer.equals("ok")){
                                    login_signup.user_username = username;
                                    startActivity(intent);
                                }else {
                                    error_textview.setText("this username is already existed");
                                }

                            }else {
                                error_textview.setText("the username is too short");
                            }



                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
                thread.start();



            }
        });



    }
}