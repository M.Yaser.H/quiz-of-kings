package com.example.quizofkings.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.example.quizofkings.R;
import com.example.quizofkings.login_signup;

import java.io.IOException;

public class Profile_fragment extends Fragment {
    public static boolean in_the_app = false;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Profile");

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.activity_profile_fragment,container,false);

        TextView usernameProfile = root.findViewById(R.id.username_textview_profile);
        TextView usernameProfile_textView = root.findViewById(R.id.username_profile);
        usernameProfile.setText(login_signup.user_username);
        final Button log_out = root.findViewById(R.id.logout_button);
        final Intent intent = new Intent(getActivity(),login_signup.class);
        log_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(intent);
                login_signup.socket.close();
                try {
                    login_signup.dos.close();
                    login_signup.dis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });


        return root;
    }

}
