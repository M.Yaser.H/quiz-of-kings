package com.example.quizofkings.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.example.quizofkings.R;
import com.example.quizofkings.Wait_for_game;
import com.example.quizofkings.login_signup;

import java.io.IOException;

public class Home_fragment extends Fragment {
    public static boolean wait_for_game = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Home");

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.activity_home_fragment,container,false);
        TextView score_textview = root.findViewById(R.id.score_textView);
        TextView coins_textview = root.findViewById(R.id.coins_textView);
        Button new_game_button = root.findViewById(R.id.new_game_button);
        Button unlimited_game_button = root.findViewById(R.id.unlimited_game_button);

        final Intent intent = new Intent(getActivity() , Wait_for_game.class);
        new_game_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            login_signup.dos.writeUTF("new_game");
                            login_signup.dos.writeUTF(login_signup.user_username);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
                thread.start();

                wait_for_game=true;
                startActivity(intent);
            }
        });

        unlimited_game_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            login_signup.dos.writeUTF("new_game");
                            login_signup.dos.writeUTF(login_signup.user_username);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
                thread.start();

                wait_for_game=true;
                startActivity(intent);
            }
        });
        return root;
    }

}
