package com.example.quizofkings.fragments;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.example.quizofkings.Change_password;
import com.example.quizofkings.Change_username;
import com.example.quizofkings.R;
import com.example.quizofkings.login_signup;

import java.io.IOException;

public class Setting_fragment extends Fragment {
    MediaPlayer mediaPlayer;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Setting");

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.activity_setting_fragment,container,false);
        Button change_username_button = root.findViewById(R.id.change_username_button);
        Button change_password_button = root.findViewById(R.id.change_password_button);
        Button play_music_button = root.findViewById(R.id.play_music_button);
        Button pause_music_button = root.findViewById(R.id.pause_music_button);
        final TextView error = root.findViewById(R.id.error_textView_setting);

        mediaPlayer = MediaPlayer.create(getActivity(),R.raw.music);
        play_music_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mediaPlayer.start();
                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mediaPlayer) {
                        mediaPlayer.start();
                    }
                });
            }
        });

        pause_music_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mediaPlayer.pause();
            }
        });

        final Intent change_username = new Intent(getActivity() , Change_username.class);
        change_username_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (login_signup.is_guest){
                    error.setText("guest users can not change username");
                }else {
                    Log.i("info","we are in else block");
                    Thread thread3 = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                login_signup.dos.writeUTF("change_username");
                                Log.i("info","message sent to server");
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    thread3.start();
                    Log.i("info","thread started");
                    startActivity(change_username);
                }

            }
        });




        final Intent change_password = new Intent(getActivity() , Change_password.class);
        change_password_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            login_signup.dos.writeUTF("change_password");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
                if (login_signup.is_guest){
                    error.setText("guest users can not change password");
                }else {
                    thread.start();
                    startActivity(change_password);
                }

            }
        });

        return root;
    }
}
