package com.example.quizofkings;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class Splash_Screen extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash__screen);

        int splash_time = 8000;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent start_intent = new Intent(Splash_Screen.this,login_signup.class);
                startActivity(start_intent);
                finish();
            }
        }, splash_time);


    }

}