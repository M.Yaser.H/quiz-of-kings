package com.example.quizofkings;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.IOException;

public class Wait_for_game extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wait_for_game);
        final EditText friend_username = findViewById(R.id.friend_username_editText);
        Button submit = findViewById(R.id.submit_button_wait_game);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String friend = friend_username.getText().toString();
                if (friend.length()>1){
                    Thread thread = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                login_signup.dos.writeUTF(friend);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    thread.start();
                }
            }
        });


    }
}